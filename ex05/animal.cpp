//
// Created by Ennio on 23/10/2019.
//

#include "animal.hpp"

/**
 * Header for Penna animal class.
 * Programming Techniques for Scientific Simulations, HS 2019
 */

#ifndef ANIMAL_HPP
#define ANIMAL_HPP

#include "genome.hpp"

/// Penna namespace.
namespace Penna {

    void Animal::set_maturity_age( const age_t R){
        maturity_age_ = R;
    };   ///< set maturity age
    void Animal::set_probability_to_get_pregnant ( const double p){
        probability_to_get_pregnant_ = p;
    }; ///< set birth rate modifier

    void Animal::set_bad_threshold( const age_t T){
        bad_threshold_ = T;
    }; ///< set threshold for deletrious mutation
    age_t Animal::get_bad_threshold() const;  ///< get deletrious mutation threshold

    Animal::Animal( const Genome& ){
        gen_ = Genome;
    };     ///< constructor using a given genome

    bool Animal::is_dead() const{
        return gen_.count_bad() >= bad_threshold_;
    };        ///< get death status
    bool Animal::is_pregnant() const{
        return pregnant_;
    };    ///< get pregnancy status
    age_t Animal::age() const{
        return age_;
    };           ///< get age

    void Animal::grow(){
        age_++;
    };                 ///< grow the animal older by one year

    Animal Animal::give_birth(){
        Animal son = Animal();
        son.gen_.mutate();
        return son;
    };         ///< create baby with inherited, randomly mutated genes

} // namespace Penna

#endif // ANIMAL_HPP

