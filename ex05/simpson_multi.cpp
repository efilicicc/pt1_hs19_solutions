#include <cmath>
#include <iostream>
#define M_E 2.7182818284590452354
#define M_R 1.7606


auto oned_integrator = [](const double a, const double b, const unsigned bins, auto oned_f) {

    const unsigned int steps = 2*bins + 1;

    const double dr = (b - a) / (steps - 1);

    double I = oned_f(a);

    for(unsigned int i = 1; i < steps-1; ++i)
        I += 2 * (1.0 + i%2) * oned_f(a + dr * i);

    I += oned_f(b);

    return I * (1./3) * dr;

};

double twod_integrator(const double a, const double b, const double c, const double d, /// intervals
        const unsigned int Nx, const unsigned int Ny,     /// bins
        auto f_twod){   /// two variable function

    return oned_integrator(a, b, Nx,
      [c, d, Ny, f_twod](const double x){
                return oned_integrator(c, d, Ny,
                  [x, f_twod](const double y){ return f_twod(x,y); }
            ); }
      );

}

int main(){

  auto square_mod = [](const double x, const double y){ return pow(x,2) + pow(y,2);};

  auto uniform_f = [square_mod](const double x, const double y){
      if( square_mod(x,y) <= 1){
          return 1.;
      }
      else {
          return 0.;
      }
  };


  auto twod_exp = [square_mod](const double x, const double y){
    if( square_mod(x,y) <= pow(M_R,2) ){
        return pow(M_E, -square_mod(x,y));
    }
    else {
        return 0.;
    };
  };


    std::cout << "Integrator maximum resolution along x?" << std::endl;
    size_t Nx;
    std::cin >> Nx;
    std::cout << "Integrator maximum resolution along y?" << std::endl;
    size_t Ny;
    std::cin >> Ny;


    std::cout << "Uniform: " << twod_integrator(-1, 1, -1, 1, Nx, Ny, uniform_f) << std::endl;
    std::cout << "Exp: " << twod_integrator(-M_R, M_R, -M_R, M_R, Nx, Ny, uniform_f) << std::endl;

}
