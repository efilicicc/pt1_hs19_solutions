/**
 * Header for Penna genome class.
 * Programming Techniques for Scientific Simulations, HS 2019
 */

#ifndef GENOME_HPP
#define GENOME_HPP

#include <bitset>
#include <limits>
#include "genome.hpp"
/// Penna namespace.
namespace Penna {

    /// Type for age of Genome holders.
    using age_t = unsigned int;

    const age_t Genome::number_of_genes =     ///< genome size
            std::numeric_limits<unsigned long>::digits;

    static void Genome::set_mutation_rate( age_t M){
        mutation_rate_ = M;
    };  ///< rate of mutation at birth

    age_t Genome::count_bad( age_t n) const {
        b = genes_;
        b >>= n;
        return b.count();
    };      ///< count number of bad genes in first n years

    void Genome::mutate(){
        for (int i = 0; i < mutation_rate_; ++i) {
            b.flip(i);
        }
    };                       ///< mutate the genome by flipping of M genes

} // namespace Penna

#endif // GENOME_HPP
